<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOrderDisplay
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOrderDisplay))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.cntxmnuSetup = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EnableAutoupdateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.timCurrentTime = New System.Windows.Forms.Timer(Me.components)
        Me.timScroll = New System.Windows.Forms.Timer(Me.components)
        Me.timScreenRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.StatusStrip2 = New System.Windows.Forms.StatusStrip()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStrip3 = New System.Windows.Forms.ToolStrip()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ToolStrip2 = New System.Windows.Forms.ToolStrip()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtNotice = New RsDesign.Controls.STD.RsTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolStrip7 = New System.Windows.Forms.ToolStrip()
        Me.timMarquee = New System.Windows.Forms.Timer(Me.components)
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.RstxtCustInfo = New RsDesign.Controls.STD.RsTextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolStrip4 = New System.Windows.Forms.ToolStrip()
        Me.ToolStrip8 = New System.Windows.Forms.ToolStrip()
        Me.ToolStrip5 = New System.Windows.Forms.ToolStrip()
        Me.ToolStrip6 = New System.Windows.Forms.ToolStrip()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabelCurrentTime = New System.Windows.Forms.ToolStripLabel()
        Me.lblRemarks = New System.Windows.Forms.ToolStripLabel()
        Me.lblAge = New System.Windows.Forms.ToolStripLabel()
        Me.RsAlertWindowError = New RsDesign.Controls.Popup.RsAlertWindow(Me.components)
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.dgvwOrderInfo = New System.Windows.Forms.DataGridView()
        Me.cntxmnuSetup.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvwOrderInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cntxmnuSetup
        '
        Me.cntxmnuSetup.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SettingsToolStripMenuItem, Me.EnableAutoupdateToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.cntxmnuSetup.Name = "cntxmnuSetup"
        Me.cntxmnuSetup.Size = New System.Drawing.Size(181, 70)
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'EnableAutoupdateToolStripMenuItem
        '
        Me.EnableAutoupdateToolStripMenuItem.Name = "EnableAutoupdateToolStripMenuItem"
        Me.EnableAutoupdateToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.EnableAutoupdateToolStripMenuItem.Text = "Enable Auto-update"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'timCurrentTime
        '
        Me.timCurrentTime.Enabled = True
        Me.timCurrentTime.Interval = 1000
        '
        'timScroll
        '
        Me.timScroll.Enabled = True
        Me.timScroll.Interval = 1000
        '
        'timScreenRefresh
        '
        Me.timScreenRefresh.Interval = 60000
        '
        'StatusStrip2
        '
        Me.StatusStrip2.AutoSize = False
        Me.StatusStrip2.BackColor = System.Drawing.Color.Ivory
        Me.StatusStrip2.Dock = System.Windows.Forms.DockStyle.Left
        Me.StatusStrip2.Location = New System.Drawing.Point(0, 0)
        Me.StatusStrip2.Name = "StatusStrip2"
        Me.StatusStrip2.Size = New System.Drawing.Size(10, 454)
        Me.StatusStrip2.SizingGrip = False
        Me.StatusStrip2.TabIndex = 84
        Me.StatusStrip2.Text = "StatusStrip2"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.AutoSize = False
        Me.StatusStrip1.BackColor = System.Drawing.Color.Ivory
        Me.StatusStrip1.Dock = System.Windows.Forms.DockStyle.Right
        Me.StatusStrip1.Location = New System.Drawing.Point(882, 0)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(10, 454)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 85
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStrip3
        '
        Me.ToolStrip3.AutoSize = False
        Me.ToolStrip3.BackColor = System.Drawing.Color.Ivory
        Me.ToolStrip3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolStrip3.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ToolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip3.Location = New System.Drawing.Point(10, 444)
        Me.ToolStrip3.Name = "ToolStrip3"
        Me.ToolStrip3.Size = New System.Drawing.Size(872, 10)
        Me.ToolStrip3.TabIndex = 87
        Me.ToolStrip3.Text = "ToolStrip3"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.PictureBox3)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(10, 25)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(872, 110)
        Me.Panel1.TabIndex = 105
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.PictureBox3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox3.Image = Global.PardiniOrderDisplay.My.Resources.Resources.Pardini_Banner
        Me.PictureBox3.Location = New System.Drawing.Point(168, 0)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(534, 108)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 4
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Dock = System.Windows.Forms.DockStyle.Right
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(702, 0)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(168, 108)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(168, 108)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'ToolStrip2
        '
        Me.ToolStrip2.AutoSize = False
        Me.ToolStrip2.BackColor = System.Drawing.Color.Ivory
        Me.ToolStrip2.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ToolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip2.Location = New System.Drawing.Point(10, 160)
        Me.ToolStrip2.Name = "ToolStrip2"
        Me.ToolStrip2.Size = New System.Drawing.Size(872, 10)
        Me.ToolStrip2.TabIndex = 110
        Me.ToolStrip2.Text = "ToolStrip2"
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel2.Controls.Add(Me.txtNotice)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.ForeColor = System.Drawing.Color.White
        Me.Panel2.Location = New System.Drawing.Point(10, 399)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(872, 45)
        Me.Panel2.TabIndex = 134
        '
        'txtNotice
        '
        Me.txtNotice.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.txtNotice.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtNotice.Border = False
        Me.txtNotice.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNotice.Cursor = System.Windows.Forms.Cursors.Hand
        Me.txtNotice.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtNotice.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotice.ForeColor = System.Drawing.Color.White
        Me.txtNotice.HideCaretIfReadOnly = True
        Me.txtNotice.Location = New System.Drawing.Point(431, 0)
        Me.txtNotice.Name = "txtNotice"
        Me.txtNotice.ReadOnly = True
        Me.txtNotice.Size = New System.Drawing.Size(437, 47)
        Me.txtNotice.TabIndex = 138
        Me.txtNotice.TabStop = False
        Me.txtNotice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNotice.ToolTip.AnimationOnClose = True
        Me.txtNotice.ToolTip.CloseOnTimer = True
        Me.txtNotice.ToolTip.Size = New System.Drawing.Size(6, 7)
        Me.txtNotice.WordWrap = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Black
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(431, 41)
        Me.Label1.TabIndex = 133
        Me.Label1.Text = "COMPLETE ORDERS :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ToolStrip7
        '
        Me.ToolStrip7.AutoSize = False
        Me.ToolStrip7.BackColor = System.Drawing.Color.Ivory
        Me.ToolStrip7.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolStrip7.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ToolStrip7.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip7.Location = New System.Drawing.Point(10, 389)
        Me.ToolStrip7.Name = "ToolStrip7"
        Me.ToolStrip7.Size = New System.Drawing.Size(872, 10)
        Me.ToolStrip7.TabIndex = 135
        Me.ToolStrip7.Text = "ToolStrip7"
        '
        'timMarquee
        '
        Me.timMarquee.Enabled = True
        Me.timMarquee.Interval = 400
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel3.Controls.Add(Me.RstxtCustInfo)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.ForeColor = System.Drawing.Color.White
        Me.Panel3.Location = New System.Drawing.Point(10, 170)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(872, 45)
        Me.Panel3.TabIndex = 138
        '
        'RstxtCustInfo
        '
        Me.RstxtCustInfo.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.RstxtCustInfo.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.RstxtCustInfo.Border = False
        Me.RstxtCustInfo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RstxtCustInfo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RstxtCustInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RstxtCustInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RstxtCustInfo.ForeColor = System.Drawing.Color.White
        Me.RstxtCustInfo.HideCaretIfReadOnly = True
        Me.RstxtCustInfo.Location = New System.Drawing.Point(286, 0)
        Me.RstxtCustInfo.Name = "RstxtCustInfo"
        Me.RstxtCustInfo.ReadOnly = True
        Me.RstxtCustInfo.Size = New System.Drawing.Size(582, 47)
        Me.RstxtCustInfo.TabIndex = 137
        Me.RstxtCustInfo.TabStop = False
        Me.RstxtCustInfo.ToolTip.AnimationOnClose = True
        Me.RstxtCustInfo.ToolTip.CloseOnTimer = True
        Me.RstxtCustInfo.ToolTip.Size = New System.Drawing.Size(6, 7)
        Me.RstxtCustInfo.WordWrap = False
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Black
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(0, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(286, 41)
        Me.Label2.TabIndex = 133
        Me.Label2.Text = "ORDER INFO:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ToolStrip4
        '
        Me.ToolStrip4.AutoSize = False
        Me.ToolStrip4.BackColor = System.Drawing.Color.Ivory
        Me.ToolStrip4.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ToolStrip4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip4.Location = New System.Drawing.Point(10, 215)
        Me.ToolStrip4.Name = "ToolStrip4"
        Me.ToolStrip4.Size = New System.Drawing.Size(872, 10)
        Me.ToolStrip4.TabIndex = 142
        Me.ToolStrip4.Text = "ToolStrip4"
        '
        'ToolStrip8
        '
        Me.ToolStrip8.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ToolStrip8.BackgroundImage = CType(resources.GetObject("ToolStrip8.BackgroundImage"), System.Drawing.Image)
        Me.ToolStrip8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ToolStrip8.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ToolStrip8.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip8.Location = New System.Drawing.Point(10, 225)
        Me.ToolStrip8.Name = "ToolStrip8"
        Me.ToolStrip8.Size = New System.Drawing.Size(872, 25)
        Me.ToolStrip8.TabIndex = 143
        Me.ToolStrip8.Text = "ToolStrip8"
        '
        'ToolStrip5
        '
        Me.ToolStrip5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ToolStrip5.BackgroundImage = CType(resources.GetObject("ToolStrip5.BackgroundImage"), System.Drawing.Image)
        Me.ToolStrip5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ToolStrip5.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ToolStrip5.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip5.Location = New System.Drawing.Point(10, 135)
        Me.ToolStrip5.Name = "ToolStrip5"
        Me.ToolStrip5.Size = New System.Drawing.Size(872, 25)
        Me.ToolStrip5.TabIndex = 109
        Me.ToolStrip5.Text = "ToolStrip5"
        '
        'ToolStrip6
        '
        Me.ToolStrip6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ToolStrip6.BackgroundImage = CType(resources.GetObject("ToolStrip6.BackgroundImage"), System.Drawing.Image)
        Me.ToolStrip6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ToolStrip6.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ToolStrip6.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip6.Location = New System.Drawing.Point(10, 0)
        Me.ToolStrip6.Name = "ToolStrip6"
        Me.ToolStrip6.Size = New System.Drawing.Size(872, 25)
        Me.ToolStrip6.TabIndex = 101
        Me.ToolStrip6.Text = "ToolStrip6"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ToolStrip1.BackgroundImage = CType(resources.GetObject("ToolStrip1.BackgroundImage"), System.Drawing.Image)
        Me.ToolStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolStrip1.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabelCurrentTime, Me.lblRemarks, Me.lblAge})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 454)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(892, 42)
        Me.ToolStrip1.TabIndex = 21
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripLabelCurrentTime
        '
        Me.ToolStripLabelCurrentTime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripLabelCurrentTime.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripLabelCurrentTime.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripLabelCurrentTime.Font = New System.Drawing.Font("Calibri", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripLabelCurrentTime.ForeColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripLabelCurrentTime.Margin = New System.Windows.Forms.Padding(0, 1, 5, 2)
        Me.ToolStripLabelCurrentTime.Name = "ToolStripLabelCurrentTime"
        Me.ToolStripLabelCurrentTime.Size = New System.Drawing.Size(371, 39)
        Me.ToolStripLabelCurrentTime.Text = "ToolStripLabelCurrentTime"
        '
        'lblRemarks
        '
        Me.lblRemarks.BackColor = System.Drawing.Color.Transparent
        Me.lblRemarks.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.lblRemarks.Font = New System.Drawing.Font("Calibri", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblRemarks.Margin = New System.Windows.Forms.Padding(5, 1, 5, 2)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(0, 39)
        '
        'lblAge
        '
        Me.lblAge.BackColor = System.Drawing.Color.Transparent
        Me.lblAge.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.lblAge.Font = New System.Drawing.Font("Calibri", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAge.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblAge.Margin = New System.Windows.Forms.Padding(10, 1, 5, 2)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(0, 39)
        '
        'RsAlertWindowError
        '
        Me.RsAlertWindowError.AnimationOnClose = True
        Me.RsAlertWindowError.AnimationType = RsDesign.Controls.Popup.RsPopupWindowAnimationType.SliderBottom
        Me.RsAlertWindowError.ImagePos = RsDesign.Controls.Popup.RsToolTipImagePos.FromText
        Me.RsAlertWindowError.Margin = New System.Windows.Forms.Padding(2)
        Me.RsAlertWindowError.ShowTime = 3000
        Me.RsAlertWindowError.Size = New System.Drawing.Size(8, 9)
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.dgvwOrderInfo)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(10, 250)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(872, 139)
        Me.Panel4.TabIndex = 144
        '
        'dgvwOrderInfo
        '
        Me.dgvwOrderInfo.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.dgvwOrderInfo.AllowUserToAddRows = False
        Me.dgvwOrderInfo.AllowUserToDeleteRows = False
        Me.dgvwOrderInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvwOrderInfo.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvwOrderInfo.BackgroundColor = System.Drawing.Color.White
        Me.dgvwOrderInfo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvwOrderInfo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvwOrderInfo.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvwOrderInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvwOrderInfo.ContextMenuStrip = Me.cntxmnuSetup
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvwOrderInfo.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvwOrderInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvwOrderInfo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvwOrderInfo.GridColor = System.Drawing.Color.Black
        Me.dgvwOrderInfo.Location = New System.Drawing.Point(0, 0)
        Me.dgvwOrderInfo.MultiSelect = False
        Me.dgvwOrderInfo.Name = "dgvwOrderInfo"
        Me.dgvwOrderInfo.ReadOnly = True
        Me.dgvwOrderInfo.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvwOrderInfo.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvwOrderInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvwOrderInfo.ShowEditingIcon = False
        Me.dgvwOrderInfo.Size = New System.Drawing.Size(870, 137)
        Me.dgvwOrderInfo.TabIndex = 145
        Me.dgvwOrderInfo.TabStop = False
        '
        'frmOrderDisplay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(892, 496)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.ToolStrip8)
        Me.Controls.Add(Me.ToolStrip4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.ToolStrip7)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.ToolStrip2)
        Me.Controls.Add(Me.ToolStrip5)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ToolStrip6)
        Me.Controls.Add(Me.ToolStrip3)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.StatusStrip2)
        Me.Controls.Add(Me.ToolStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmOrderDisplay"
        Me.ShowIcon = False
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.cntxmnuSetup.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        CType(Me.dgvwOrderInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cntxmnuSetup As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents timCurrentTime As System.Windows.Forms.Timer
    Friend WithEvents timScroll As System.Windows.Forms.Timer
    Friend WithEvents timScreenRefresh As System.Windows.Forms.Timer
    Friend WithEvents EnableAutoupdateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip2 As System.Windows.Forms.StatusStrip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStrip3 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStrip6 As System.Windows.Forms.ToolStrip
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents ToolStrip5 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStrip2 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripLabelCurrentTime As System.Windows.Forms.ToolStripLabel
    Friend WithEvents lblRemarks As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents lblAge As System.Windows.Forms.ToolStripLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ToolStrip7 As System.Windows.Forms.ToolStrip
    Friend WithEvents timMarquee As System.Windows.Forms.Timer
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ToolStrip4 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStrip8 As System.Windows.Forms.ToolStrip
    Friend WithEvents RsAlertWindowError As RsDesign.Controls.Popup.RsAlertWindow
    Friend WithEvents txtNotice As RsDesign.Controls.STD.RsTextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents dgvwOrderInfo As System.Windows.Forms.DataGridView
    Friend WithEvents RstxtCustInfo As RsDesign.Controls.STD.RsTextBox

End Class
