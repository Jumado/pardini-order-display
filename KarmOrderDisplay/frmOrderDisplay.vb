Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Text
Imports System.Drawing
Imports System.Data
Public Class frmOrderDisplay
    Dim CompleteOrderstring As New StringBuilder
    Dim OrderInfo As StringBuilder
    Dim TblOrderInfo As DataTable
    Dim WithEvents BgwkRetrieveOrders As New BackgroundWorker
    Dim WithEvents BgwkCustomerInfo As New BackgroundWorker With {.WorkerReportsProgress = True, .WorkerSupportsCancellation = True}
    Dim CurrentRec As Integer = 0
    Dim OrderDict As New Dictionary(Of Integer, String)

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub SettingsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SettingsToolStripMenuItem.Click
        frmSettings.Show()

    End Sub

    Private Sub BgwkRetrieveOrders_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BgwkRetrieveOrders.DoWork
        Try
            Dim SqlConnectionstr As New SqlConnectionStringBuilder
            SqlConnectionstr.DataSource = My.Settings.ServerName.Trim
            SqlConnectionstr.UserID = My.Settings.DBUsername.Trim
            SqlConnectionstr.InitialCatalog = My.Settings.DBName.Trim
            SqlConnectionstr.Password = My.Settings.ServerPassword.Trim
            Dim DBConnOrder As New SqlConnection(SqlConnectionstr.ToString)

            Try

                If (DBConnOrder.State = ConnectionState.Closed) Then
                    DBConnOrder.Open()
                End If
                Dim RecNum As Integer = 0
                Dim CMDOrders As New SqlCommand
                CMDOrders.Connection = DBConnOrder
                CMDOrders.CommandText = "pdProcOpenOrders"
                CMDOrders.CommandType = CommandType.StoredProcedure
                Dim Reader As SqlDataReader = CMDOrders.ExecuteReader
                While Reader.Read
                    If Not (DBNull.Value.Equals(Reader!OrderNo)) Then
                        RecNum += 1
                        Me.BgwkRetrieveOrders.ReportProgress(RecNum, IIf(DBNull.Value.Equals(Reader!OrderNo), String.Empty, Reader!OrderNo))
                    End If
                End While

            Catch ex As Exception
                e.Result = "error"
                Me.BgwkRetrieveOrders.CancelAsync()
            Finally
                If (DBConnOrder.State = ConnectionState.Closed) Then
                    DBConnOrder.Open()
                End If
            End Try
        Catch ex As Exception
        End Try
    End Sub

   
    Private Sub frmOrderDisplay_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Me.timScreenRefresh.Interval = CInt((My.Settings.UpdateInterval) * 60000)
        Catch ex As Exception
        Finally
            Me.BgwkRetrieveOrders.WorkerReportsProgress = True
            Me.BgwkRetrieveOrders.WorkerSupportsCancellation = True
            Me.BgwkRetrieveOrders.RunWorkerAsync()
        End Try

    End Sub

    Private Sub BgwkRetrieveOrders_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BgwkRetrieveOrders.ProgressChanged
        Try
            Me.OrderDict.Add(CInt(e.ProgressPercentage), TryCast(e.UserState, String))

        Catch ex As Exception

        End Try
    End Sub

   



    Private Sub timCurrentTime_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timCurrentTime.Tick
        Me.ToolStripLabelCurrentTime.Text = Now.ToString("dddd  MMMM d,yyyy" & Space(3) & "h:mm:ss tt").ToUpper
    End Sub






    Private Sub EnableAutoupdateToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EnableAutoupdateToolStripMenuItem.Click
        If Not (Me.BgwkRetrieveOrders.IsBusy) Then
            Me.timScreenRefresh.Enabled = True
            MsgBox("Auto-update enabled", MsgBoxStyle.OkOnly + MsgBoxStyle.ApplicationModal + MsgBoxStyle.Information, "Auto-update")
            TryCast(sender, ToolStripMenuItem).Checked = True
        Else
            MsgBox("Loading orders", MsgBoxStyle.OkOnly + MsgBoxStyle.ApplicationModal + MsgBoxStyle.Information, "Please wait")

        End If
    End Sub

   
    Private Sub BgwkCustomerInfo_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BgwkCustomerInfo.DoWork
        Try
            Dim LiCollection As New ArrayList
            Dim SqlConnectionstr As New SqlConnectionStringBuilder
            SqlConnectionstr.DataSource = My.Settings.ServerName.Trim
            SqlConnectionstr.UserID = My.Settings.DBUsername.Trim
            SqlConnectionstr.InitialCatalog = My.Settings.DBName.Trim
            SqlConnectionstr.Password = My.Settings.ServerPassword.Trim
            Dim DBConnOrder As New SqlConnection(SqlConnectionstr.ToString)
            Try
                If (DBConnOrder.State = ConnectionState.Closed) Then
                    DBConnOrder.Open()
                End If

                Dim CMDOrderInfo As New SqlCommand
                CMDOrderInfo.Connection = DBConnOrder
                CMDOrderInfo.CommandText = "pdProcOrderInfo"
                CMDOrderInfo.CommandType = CommandType.StoredProcedure
                CMDOrderInfo.Parameters.Clear()
                CMDOrderInfo.Parameters.AddWithValue("@OrderNo", TryCast(e.Argument, String))
                Dim Reader As SqlDataReader
                Reader = CMDOrderInfo.ExecuteReader
                Dim CustInfo As New StringBuilder
                While Reader.Read
                    CustInfo.Append("<<<>>>" & Space(2))
                    If Not (IsDBNull(Reader!OrderNo)) Then
                        CustInfo.Append("Order number: " & Reader!OrderNo & Space(2) & "<<<>>>" & Space(2))
                    End If
                    If Not (IsDBNull(Reader!Customer)) Then
                        CustInfo.Append("Customer: " & Reader!Customer & Space(2) & "<<<>>>" & Space(2))
                    End If
                    If Not (IsDBNull(Reader!CommitDate)) Then
                        CustInfo.Append("Commit date: " & Date.Parse(Reader!CommitDate).ToString("dd-MM-yyyy") & Space(2) & "<<<>>>" & Space(2))
                    End If
                    If Not (IsDBNull(Reader!OrderDate)) Then
                        CustInfo.Append("Order date: " & Date.Parse(Reader!OrderDate).ToString("dd-MM-yyyy") & Space(2) & "<<<>>>" & Space(2))
                    End If
                    If (Not (IsDBNull(Reader!ProdMonth))) And (Not (IsDBNull(Reader!ProdYear))) Then
                        CustInfo.Append("Production month : " & MonthName(Reader!ProdMonth).ToString & " , " & Convert.ToString(Reader!ProdYear) & Space(2) & "<<<>>>" & Space(2))
                    End If
                End While
                Reader.Close()
                Me.BgwkCustomerInfo.ReportProgress(0, CustInfo)
                CMDOrderInfo.CommandText = "pdProcPardiniDisplay"
                CMDOrderInfo.CommandType = CommandType.StoredProcedure
                CMDOrderInfo.Parameters.Clear()
                CMDOrderInfo.Parameters.AddWithValue("@OrderNo", TryCast(e.Argument, String))
                Dim DAOderInfo As New SqlDataAdapter With {.SelectCommand = CMDOrderInfo}
                Dim DSOrderInfo As New DataSet
                DAOderInfo.Fill(DSOrderInfo, "OrderInfo")
                e.Result = DSOrderInfo.Tables!OrderInfo
            Catch ex As Exception
                e.Result = "error"
                Me.BgwkCustomerInfo.CancelAsync()
            Finally
                If (DBConnOrder.State = ConnectionState.Open) Then
                    DBConnOrder.Close()
                End If
            End Try
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BgwkCustomerInfo_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BgwkCustomerInfo.ProgressChanged
        Try
            Me.RstxtCustInfo.Text = TryCast(e.UserState, StringBuilder).ToString.Trim
            Me.OrderInfo = TryCast(e.UserState, StringBuilder)
            If Not IsNothing(Me.TblOrderInfo) Then
                Me.TblOrderInfo.Clear()
                Me.dgvwOrderInfo.Refresh()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function MarqueeLeft(ByVal Text As String)
        Dim Str1 As String = Text.Remove(0, 1)
        Dim Str2 As String = Text(0)
        Return Str1 & Str2
    End Function


    Private Sub timMarque_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timMarquee.Tick

        Try

            If Not (Me.RstxtCustInfo.Text.Trim = String.Empty) And ((Me.RstxtCustInfo.TextLength < Me.OrderInfo.ToString.Length)) Then
                Me.RstxtCustInfo.Text = Me.MarqueeLeft(Me.RstxtCustInfo.Text)
            End If
            If Not (Me.txtNotice.Text.Trim = String.Empty) And (Me.txtNotice.TextLength < Me.CompleteOrderstring.ToString.Length) Then
                Me.txtNotice.Text = MarqueeLeft(Me.txtNotice.Text)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub timScreenRefresh_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timScreenRefresh.Tick
        Try
            If Not (Me.OrderDict.Count < Me.CurrentRec) Then
                If Not (Me.BgwkCustomerInfo.IsBusy Or Me.BgwkRetrieveOrders.IsBusy) Then
                    Me.CurrentRec += 1
                    Me.BgwkCustomerInfo.RunWorkerAsync(OrderDict.Item(Me.CurrentRec).Trim)
                    Me.timScreenRefresh.Enabled = False
                End If

            Else
                Me.OrderDict.Clear()
                Me.CurrentRec = 0
                If Not (Me.BgwkRetrieveOrders.IsBusy) Then
                    Me.BgwkRetrieveOrders.RunWorkerAsync()
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BgwkRetrieveOrders_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BgwkRetrieveOrders.RunWorkerCompleted
        If Not (Me.OrderDict.Count = 0) Then
            Me.timScreenRefresh.Enabled = True
            Me.CompleteOrderstring.Clear()
            Me.CurrentRec = 0
        End If
        Try
            If (TryCast(e.Result, String) = "error") Then
                Me.RsAlertWindowError.Text = "Failed to retrieve orders"
                Me.RsAlertWindowError.Image = My.Resources.Warning
                Me.RsAlertWindowError.Caption = "Connection error"
                Me.RsAlertWindowError.Show()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BgwkCustomerInfo_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BgwkCustomerInfo.RunWorkerCompleted
        Try

            If (TryCast(e.Result, String) = "error") Then
                Me.RsAlertWindowError.Text = "Failed to retrieve order items"
                Me.RsAlertWindowError.Image = My.Resources.Warning
                Me.RsAlertWindowError.Caption = "Connection or data error"
                Me.RsAlertWindowError.Show()
            End If
            TblOrderInfo = TryCast(e.Result, DataTable)
            Me.dgvwOrderInfo.DataSource = TblOrderInfo
            Me.dgvwOrderInfo.CurrentCell = Nothing
            Me.timScreenRefresh.Enabled = True
        Catch ex As Exception
        Finally
            Dim RsImageCol As New DataGridViewImageColumn With {.HeaderText = "VISUAL MGT", .Name = "ColSign"}
            If (Me.dgvwOrderInfo.Columns.Contains(RsImageCol.Name)) Then
                Me.dgvwOrderInfo.Columns.Remove(RsImageCol.Name)
            End If
            Me.dgvwOrderInfo.Columns.Insert(1, RsImageCol)
            Dim OrderComplete As Boolean = True
            For Each RsdgvRow As DataGridViewRow In Me.dgvwOrderInfo.Rows
                If Val(RsdgvRow.Cells(6).Value) < 0 Then
                    dgvwOrderInfo.Rows(RsdgvRow.Index).DefaultCellStyle.BackColor = Color.Red
                    dgvwOrderInfo.Item(1, RsdgvRow.Index).Value = New Bitmap(My.Resources.UserStop)
                    OrderComplete = False
                ElseIf (Val(RsdgvRow.Cells(6).Value)) > 0 Then
                    RsdgvRow.DefaultCellStyle.BackColor = Color.DarkGoldenrod
                    dgvwOrderInfo.Item(1, RsdgvRow.Index).Value = New Bitmap(My.Resources.Warning)
                    OrderComplete = False
                Else
                    dgvwOrderInfo.Item(1, RsdgvRow.Index).Value = New Bitmap(My.Resources.Checked)
                End If

            Next

           
            If (OrderComplete) Then
                Me.CompleteOrderstring.Append("<<<>>>" & Me.OrderDict(CurrentRec).Trim & "<<<>>>")
                Me.txtNotice.Text = Me.CompleteOrderstring.ToString
            End If


        End Try
    End Sub

    Private Sub timScroll_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timScroll.Tick
        If Not (Me.dgvwOrderInfo.RowCount <= 0) Then
            Try
                Static upward As Boolean = False
                Dim dgv As DataGridView = Me.dgvwOrderInfo
                Dim firstRowIndex As Integer = dgv.FirstDisplayedScrollingRowIndex
                Dim nrowdisplayed As Integer = dgv.DisplayedRowCount(False)

                If upward Then
                    If (dgv.FirstDisplayedScrollingRowIndex - dgv.DisplayedRowCount(False)) > 0 Then
                        dgv.FirstDisplayedScrollingRowIndex -= dgv.DisplayedRowCount(False)
                    Else
                        dgv.FirstDisplayedScrollingRowIndex = 0
                    End If
                    If (dgv.FirstDisplayedScrollingRowIndex = 0) Then upward = False
                Else
                    If (dgv.FirstDisplayedScrollingRowIndex + dgv.DisplayedRowCount(False)) < dgv.Rows.Count Then
                        dgv.FirstDisplayedScrollingRowIndex += dgv.DisplayedRowCount(False)
                    Else
                        upward = True
                    End If

                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

   
    Private Sub dgvwOrderInfo_RowsAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsAddedEventArgs)
        Try
            Me.dgvwOrderInfo.CurrentCell = Nothing


        Catch ex As Exception

        End Try
    End Sub
   
End Class
